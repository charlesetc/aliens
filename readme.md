
# Aliens

## Install

I think go get should work:

```
go get gitlab.com/charlesetc/aliens
```

If not:

```
mkdir -p $GOPATH/src/gitlab.com/charlesetc/aliens
git clone https://gitlab.com/charlesetc/aliens $GOPATH/src/gitlab.com/charlesetc/aliens
cd $GOPATH/src/gitlab.com/charlesetc/aliens
make
```

## Progression of code

I first though it'd be better to have one goroutine per city
and the cities toss around the aliens. I didn't like that at
the time and then changed it to have the aliens communicate
with the cities. This turned out to introduce a lot of race
conditions, but I also improved the surrounding code, so now
I have a solution I quite like.

Each city waits for an alien. If an alien comes, they save it
as their "current alien." Then they wait a bit of time, and
send it off to the next city. If another alien comes while the
city still has an alien, then the city is destroyed and
neither alien is sent onwards.

Since there is only one goroutine per city, it processes each
of the city's events one-at-a-time. So, even though many
cities will be passing around aliens all in parallel, each
city can reason about the aliens it is passed sequentially.

This also means that you can think of certain data as being
"owned" by a city. For example, when a city has an alien, that
alien can be mutated (changed to 'dead'  for example) without
having to worry about another city mutating it at the same
time. All without locks.

## Testing

Run with `make`. Run the tests with `make test`.
If a test goes wrong `go test ./src -run $TestName` is
very useful to isolate debug information.

I made the tests mostly integration tests based on timing
how long a test takes. This has it's advantages -- mostly
that we're able to make assertions based on how a simulation
should perform with a certain number of aliens. If the code
is wrong, for example, if it let one alien destroy two cities,
then the assertions would not pass. The disadvantage is that
code is never guaranteed to run in a given amount of time.
That said, I've run the latest test suite 80 times in a row
without failure so it's definitely usable.

## Variations from Spec

* So I think the spec implied that the aliens should move one
  at a time. (See "Each iteration".) This seems unlikely for
  an alien invasion where the aliens are hostile to each
  other. So I, wanting to simulate the impending alien
  invasion as accurately as possible, made the aliens
  move in parallel. This made for a lot more challenges, and
  but also made for a fun task using Go's channels.

* I also made the aliens sleep for a small amount of time in
  each city so they have a higher chance of running into
  another alien during that time. This means it takes around
  30 seconds for an alien to move 10,000 times, so I have the
  limit set to 1,000. See the MAX_MOVES parameter.

* I also have the names of each alien generated from
  names.txt.

## Assumptions

* There can't be more than twice as many aliens as cities. The
  first 2n aliens will destroy all the cities, and the rest
  will spend eternity trying to warp into destroyed cities,
  failing, and then trying again.

## names.txt

I generated names.txt with

```
cat /usr/share/dict/words | ag "^...$" > names.txt
```

and then in vim removing words that contained an apostrophe, making
everything lowercase, and then making all the first letters
uppercase.

This made for a small possibility of very offensive Alien
names so I deleted some of them.

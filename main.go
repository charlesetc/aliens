package main

import (
	"io/ioutil"
	"os"
	"strconv"

	aliens "gitlab.com/charlesetc/aliens/src"
	"gitlab.com/charlesetc/aliens/utils"
)

func main() {
	mapFilename := os.Args[1]
	n, err := strconv.Atoi(os.Args[2])
	utils.Check(err)

	fileContent, err := ioutil.ReadFile(mapFilename)
	utils.Check(err)

	aliens.Simulate(n, aliens.ParseCities(string(fileContent)))
}

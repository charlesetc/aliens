package aliens

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/charlesetc/aliens/utils"
)

func TestCityParsing(t *testing.T) {
	cities := ParseCities("NY north=Halifax\nHalifax south=NY")
	// t.Error(cities)
	assert.Equal(t, len(cities), 2, "it should parse the right length")

	ny := cities["NY"]

	halifax := cities["Halifax"]

	assert.Equal(t, ny.name, "NY", "it should parse the right name")
	assert.Equal(t, halifax.name, "Halifax", "it should parse the right name")

	assert.Equal(t, *ny.north, "Halifax", "it should parse the right directions")
	assert.Equal(t, *halifax.south, "NY", "it should parse the right directions")

	assert.Nil(t, halifax.west, "it should parse the nil directions")
	assert.Nil(t, halifax.east, "it should parse the nil directions")
	assert.Nil(t, halifax.north, "it should parse the nil directions")
	assert.Nil(t, ny.west, "it should parse the nil directions")
	assert.Nil(t, ny.east, "it should parse the nil directions")
	assert.Nil(t, ny.south, "it should parse the nil directions")
}

/// Since there are exactly 2n aliens for n cities,
/// they should all collide and destroy all cities.
func TestMaximalAliensDestroyAllCities(t *testing.T) {
	fileContent, err := ioutil.ReadFile("../map.txt")
	utils.Check(err)

	cities := ParseCities(string(fileContent))
	Simulate(12, cities)

	for _, city := range cities {
		assert.True(t, city.destroyed, "all cities should be destroyed")
	}
}

/// Since there is only one alien, it will wander
/// around until it gets exhausted.
func TestMinimialAliensFinishesSlow(t *testing.T) {
	fileContent, err := ioutil.ReadFile("../map.txt")
	utils.Check(err)

	finish := make(chan bool)

	go func() {
		Simulate(1, ParseCities(string(fileContent)))
		finish <- true
	}()
	go func() {
		time.Sleep(1 * time.Second)
		finish <- false
	}()
	assert.False(t, <-finish, "the simulation should run slower than 1 seconds")
}

/// Since there are two aliens, they should run
/// into each other quickly.
///
/// This one is 600ms instead of 100 ms because
/// most of the time the two aliens fall in separate
/// cities and have to wonder around a bit.
func TestTwoAliensFinishesFast(t *testing.T) {
	fileContent, err := ioutil.ReadFile("../map.txt")
	utils.Check(err)

	finish := make(chan bool)

	go func() {
		Simulate(2, ParseCities(string(fileContent)))
		finish <- true
	}()
	go func() {
		time.Sleep(600 * time.Millisecond)
		finish <- false
	}()
	assert.True(t, <-finish, "the simulation should run faster than 600 milliseconds")
}

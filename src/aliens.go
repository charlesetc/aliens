package aliens

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/charlesetc/aliens/utils"
)

const namefile = "names.txt"
const MAX_MOVES = 100

var rng *rand.Rand
var nameSegments []string
var cityNames []string

var possibleDescriptions []string = []string{
	"leveled",
	"destroyed",
	"utterly and completely destroyed",
	"wrecked",
	"ever-so-slightly dirtied",
	"completely flattened",
	"blobbed",
	"totally ****ed",
	"ENDED",
	"nuked",
	"vaporized",
	"burned to the ground",
	"terraformed into a frozen wasteland",
	"exsanguinated",
	"defenestrated",
	"teleported into the atmosphere",
	"shoved under the earth's crust",
	"covered in white hot magma",
	"███████ed by ███████ and horribly ██████████ed",
}

func init() {
	// seed the rng
	source := rand.NewSource(time.Now().UnixNano())
	// use this line instead if you want a deterministic rng
	// source := rand.NewSource(0)
	rng = rand.New(source)

	// read and set alien name segments
	var fileContent []byte
	var err error

	// if it's run from the testing command, look
	// one directory back.
	if flag.Lookup("test.v") == nil {
		fileContent, err = ioutil.ReadFile(namefile)
	} else {
		fileContent, err = ioutil.ReadFile("../" + namefile)
	}
	utils.Check(err)

	nameSegments = strings.Split(string(fileContent), "\n")
}

func log(message ...interface{}) {
	message = append([]interface{}{"\033[1;32m-\033[0m"}, message...)
	// comment out this line if you only want the city-destroyed messages!
	fmt.Println(message...)
}

type alien struct {
	number     int
	name       string
	dead       bool
	timesMoved int
}

type city struct {
	name  string
	north *string
	south *string
	east  *string
	west  *string

	destroyed bool
	sendAlien chan sendMessage
	moveAlien chan bool
}

type sendMessage struct {
	alien   *alien
	success chan bool
}

func (c *city) destroyedState() {
	log(c.name, "entered a destroyed state")
	for {
		select {
		// a third alien piled onto a city before this city could process them.
		// the first two fought, leaving nothing but barren wasteland.
		// we must tell the next alien to stay in its previous city because
		// this one does not, in fact, exist intact.
		case sendMessage := <-c.sendAlien:
			sendMessage.success <- false
		case <-c.moveAlien:
			// do nothing
		}
	}
}

func (c *city) dispatch(cities map[string]*city) {
	var currentAlien *alien
	for {
		select {
		case sendMessage := <-c.sendAlien:
			alien := sendMessage.alien
			sendMessage.success <- true
			alien.timesMoved += 1

			if alien.timesMoved > MAX_MOVES {
				alien.dead = true
				// a destroyed state but
				log(alien.name, "has died from exhaustion")

				// the city is not actually marked as "destroyed"
				// here because no aliens fought.
				c.destroyedState()
				panic("the above function never exits")
			}

			log(alien.name, "arrived in", c.name)

			if currentAlien != nil {
				c.destroyed = true
				fmt.Printf(
					"\033[1;31m!\033[0m %s has been %s by %s and %s!\n",
					c.name,
					possibleDescriptions[rng.Intn(len(possibleDescriptions))],
					alien.name,
					currentAlien.name,
				)

				// both aliens die
				alien.dead = true
				currentAlien.dead = true
				currentAlien = nil
				c.destroyedState()
				panic("the above function never exits")
			}

			// the alien has no competition, so we mark it as ours
			// and sleep
			currentAlien = alien
			go func() {
				time.Sleep(time.Duration(rng.Intn(50)+1) * time.Millisecond)
				c.moveAlien <- true
			}()

		case <-c.moveAlien:
			if currentAlien == nil {
				panic("in this case, the city should be destroyed")
				continue
			}
			for {
				direction := c.randomDirection(cities)
				if direction == nil {
					log(fmt.Sprintf("%s is stuck in %s.", currentAlien.name, c.name))
					currentAlien.dead = true // alien is effectively dead
					c.destroyedState()
					panic("the above function never exits")
				}
				nextCity := cities[*direction]
				success := make(chan bool)
				nextCity.sendAlien <- sendMessage{alien: currentAlien, success: success}
				if <-success {
					currentAlien = nil
					break
				} // otherwise try a different random direction
			}
		}
	}
}

func (c city) randomDirection(cities map[string]*city) *string {
	actually_has_a_solution := (c.north != nil && !cities[*c.north].destroyed) ||
		(c.south != nil && !cities[*c.south].destroyed) ||
		(c.east != nil && !cities[*c.east].destroyed) ||
		(c.west != nil && !cities[*c.west].destroyed)

	if !actually_has_a_solution {
		return nil
	}

	for {
		switch rng.Intn(4) {
		case 0:
			if c.north != nil && !cities[*c.north].destroyed {
				return c.north
			}
		case 1:
			if c.south != nil && !cities[*c.south].destroyed {
				return c.south
			}
		case 2:
			if c.east != nil && !cities[*c.east].destroyed {
				return c.east
			}
		case 3:
			if c.west != nil && !cities[*c.west].destroyed {
				return c.west
			}
		}
	}
	panic("The loop should return before it gets here.")
}

func ParseCities(fileContent string) map[string]*city {
	map_lines := strings.Split(fileContent, "\n")
	cities := make(map[string]*city)

	for _, line := range map_lines {
		if line == "" {
			continue
		}

		words := strings.Split(line, " ")
		cityName := words[0]

		var city city
		city.name = cityName

		for _, word := range words[1:] {
			parsed := strings.Split(word, "=")
			direction := parsed[0]
			directionCityName := parsed[1]
			switch direction {
			case "north":
				city.north = &directionCityName
			case "south":
				city.south = &directionCityName
			case "east":
				city.east = &directionCityName
			case "west":
				city.west = &directionCityName
			}
		}
		cities[cityName] = &city
		cityNames = append(cityNames, cityName)
	}
	return cities
}

func randomAlienName() string {
	first := nameSegments[rng.Intn(len(nameSegments))]
	second := nameSegments[rng.Intn(len(nameSegments))]
	return first + strings.ToLower(second)
}

func allAreDead(aliens []*alien) (out bool) {
	out = true
	for _, alien := range aliens {
		if !alien.dead {
			// log(alien.name, "is still alive")
			out = false
		}
	}
	return
}

func Simulate(n int, cities map[string]*city) {
	aliens := make([]*alien, n)

	// get cities ready to recieve aliens
	for _, city := range cities {
		city.sendAlien = make(chan sendMessage)
		city.moveAlien = make(chan bool)
		go city.dispatch(cities)
	}

	// create aliens
	for i := 0; i < n; i++ {
		a := &alien{
			number: i,
			name:   randomAlienName(),
		}
		aliens[i] = a

		for {
			city := cities[cityNames[rng.Intn(len(cityNames))]]
			success := make(chan bool)
			city.sendAlien <- sendMessage{alien: a, success: success}
			if <-success {
				break
			} // otherwise repeat
		}
	}

	for !allAreDead(aliens) {
		time.Sleep(100 * time.Millisecond)
	}

	printFormatted(cities)
}

func printFormatted(cities map[string]*city) {
	for _, city := range cities {
		if city.destroyed {
			continue
		}
		fmt.Print(city.name, " ")
		if city.north != nil && !cities[*city.north].destroyed {
			fmt.Print("north=")
			fmt.Print(*city.north, " ")
		}
		if city.south != nil && !cities[*city.south].destroyed {
			fmt.Print("south=")
			fmt.Print(*city.south, " ")
		}
		if city.east != nil && !cities[*city.east].destroyed {
			fmt.Print("east=")
			fmt.Print(*city.east, " ")
		}
		if city.west != nil && !cities[*city.west].destroyed {
			fmt.Print("west=")
			fmt.Print(*city.west, " ")
		}
		fmt.Print("\n")
	}
}
